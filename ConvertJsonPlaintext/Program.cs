﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertJsonPlaintext
{
    public class Program
    {
        public static string txtTempName { get; set; }
        static void Main(string[] args)
        {
            txtTempName = "";
            Console.WriteLine("type -r for reset");
            while (true)
            {
                try
                {                   
                    string input = Console.ReadLine();
                    //string rslt = "";
                    if (string.IsNullOrEmpty(input))
                    {
                        Console.WriteLine("Please input directory and flag");
                    }
                    else
                    {
                        if(input == "-r")
                        {
                            txtTempName = "";
                        }
                        else
                        {
                            var result = logic.Convert(input, txtTempName);

                            if (!string.IsNullOrEmpty(result.Item2))
                            {
                                Console.WriteLine(result.Item2);
                            }

                            txtTempName = result.Item1;
                        }
                    }

                    Console.Write("\n");
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                    Console.ReadLine();
                    continue;
                }
            }
        }
    }
}
