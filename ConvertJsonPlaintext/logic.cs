﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertJsonPlaintext
{
    public sealed class logic
    {
        public logic() { }

        public static Tuple<string, string> Convert(string txtInput, string txtTempName)
        {
            var splitspace = txtInput.Split(' ');
            if (splitspace.Where(x => x.Contains("-t")).Count() > 0)
            {
                return FlagTorNull(txtInput, txtTempName);
            }

            if (splitspace.Where(x => x.Contains("-o")).Count() > 0)
            {
                string g = txtInput + "==";
                g = g.Replace("==", "");
                g = g.Replace("-o", "=");
                var split = g.Split('=');

                string source = split[0];
                string target = split[split.Length - 1];

                var splitTarget = target.Split('/');
                if(splitTarget[splitTarget.Length - 1].Contains(".txt") || splitTarget[splitTarget.Length - 1].Contains(".json"))
                {
                    txtTempName = splitTarget[splitTarget.Length - 1];
                    StreamReader srText = new StreamReader(source);
                    string text = System.IO.File.ReadAllText(source);

                    createFile(txtTempName, text);
                }

                //get file di temporary txtTempName
                string path = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\Temp\\" + txtTempName;
                if (!File.Exists(path))
                {
                    return Tuple.Create(txtTempName, "File not created, please flag -t for create file");
                }

                //pindahin file ke target directory
                var dirTarget = target.Replace(txtTempName, "");
                if (!Directory.Exists(dirTarget))
                {
                    Directory.CreateDirectory(dirTarget);
                }
                File.Copy(path, target, true);
                return Tuple.Create("", "file created, command has been reset");
            }

            if (splitspace.Where(x => x.Contains("-h")).Count() > 0)
            {
                var petunjuk = "1. path_source -t type" + Environment.NewLine +
                               "--- example : C:/error.log -t json" + Environment.NewLine +
                               "2. path_source -o path_target" + Environment.NewLine +
                               "--- example : C:/error.log -0 C:/Result/error.json" + Environment.NewLine +
                               "3. path_source -t type -o path_target" + Environment.NewLine +
                               "--- example : C:/error.log -t json C:/Result/error.json" + Environment.NewLine;

                return Tuple.Create(txtTempName, petunjuk);
            }

            if (!string.IsNullOrEmpty(splitspace[0]))
            {
                //check file nya dulu
                if (File.Exists(splitspace[0]))
                {
                    return FlagTorNull(txtInput, txtTempName);
                }
            }

            return Tuple.Create(txtTempName, "i'm can't read your command");
        }

        public static Tuple<string, string> FlagTorNull(string txtInput,string txtTempName)
        {
            string g = txtInput + "==";
            g = g.Replace("==", "");
            g = g.Replace("-t", "=");
            var split = g.Split('=');
            string direct = split[0];
            string type = "";
            if(split.Length > 1) { type = split[1]; }

            type = type.Split(' ')[0];

            if (type == "json")
            {
                type = ".jsn";

            }
            else if (type == "text")
            {
                type = ".txt";
            }
            else if (string.IsNullOrEmpty(type))
            {
                type = ".txt";
            }

            if (string.IsNullOrEmpty(txtTempName))
            {

                string directoryLog = direct;
                StreamReader srText = new StreamReader(directoryLog);
                string text = System.IO.File.ReadAllText(directoryLog);

                txtTempName = DateTime.Now.ToString("ddMMyyhhmmss") + type;
                //save di directory temporary
                createFile(txtTempName, text);
            }

            return Tuple.Create(txtTempName, "");
        }

        public static void createFile(string filename, string text)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\Temp\\";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\Temp\\" + filename;
            using (FileStream fileStream = File.Open(path, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter streamWriter = new StreamWriter((Stream)fileStream))
                    streamWriter.Write((object)text);
            }
        }
    }
}
